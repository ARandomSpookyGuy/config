call plug#begin("~/.vim/plugged")

	Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	Plug 'vim-airline/vim-airline-themes'
	Plug 'peitalin/vim-jsx-typescript'
	Plug 'leafgarland/typescript-vim'
	Plug 'maxmellon/vim-jsx-pretty'
	Plug 'preservim/nerdcommenter'
	Plug 'vim-airline/vim-airline'
	Plug 'ryanoasis/vim-devicons'
	Plug 'jiangmiao/auto-pairs'
	Plug 'frazrepo/vim-rainbow'
	Plug 'scrooloose/nerdtree'
	Plug 'junegunn/fzf.vim'
	Plug 'morhetz/gruvbox'
	"Plug 'sainnhe/gruvbox-material'
	Plug 'dylanaraps/wal.vim'
	Plug 'sainnhe/gruvbox-material'
call plug#end()

"#######################################################
"Coc
	let g:coc_global_extensions = ['coc-emmet','coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver','coc-sh','coc-clangd']

"Gruv
	"let g:gruvbox_italic=1
	"colorscheme gruvbox

"Wal
	colorscheme wal 

"syntax
	au BufEnter * :syntax on 
"syntax on


"Rainbow ()
	let g:rainbow_active = 1

"unicode symbols

	let g:airline_powerline_fonts = 1

	if !exists('g:airline_symbols')
	    let g:airline_symbols = {}
	endif

	let g:airline_left_sep = '»'
	let g:airline_left_sep = '▶'
	let g:airline_right_sep = '«'
	let g:airline_right_sep = '◀'
	let g:airline_symbols.linenr = '␊'
	let g:airline_symbols.linenr = '␤'
	let g:airline_symbols.linenr = '¶'
	let g:airline_symbols.branch = '⎇'
	let g:airline_symbols.paste = 'ρ'
	let g:airline_symbols.paste = 'Þ'
	let g:airline_symbols.paste = '∥'
	let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
	let g:airline_left_sep = ''
	let g:airline_left_alt_sep = ''
	let g:airline_right_sep = ''
	let g:airline_right_alt_sep = ''
	let g:airline_symbols.branch = ''
	let g:airline_symbols.readonly = ''
	let g:airline_symbols.linenr = ''

"Airline
	"let g:airline_theme='zenburn'


"- FOLDING --
	autocmd BufWinLeave *.* mkview
	autocmd BufWinEnter *.* silent loadview 
	au BufWinEnter * set foldmethod=manual 

"Enable Mouse Control
	set mouse=a
	set noshowmode
"Copy
	vmap <C-c> "+y

"Nerdtree	
	let g:NERDTreeShowHidden = 1
	"let g:NERDTreeMinimalUI = 1
	let g:NERDTreeIgnore = []
	let g:NERDTreeStatusline = ''
	
"Toggle
	nnoremap <silent> <C-b> :NERDTreeToggle<CR>

"open new split panes to right and below
	set splitright
	set splitbelow

"turn terminal to normal mode with escape

	"tnoremap <Esc> <C-\><C-n>

"start terminal in insert mode

	"au BufEnter * if &buftype == 'terminal' | :startinsert | endif
	"au BufEnter *

"NumberLine
	set nu
	set relativenumber

"open terminal on ctrl+n
	function! OpenTerminal()
	  split term://bash
	  resize 8
	endfunction
	nnoremap <c-n> :call OpenTerminal()<CR>


"Functions 

"--Save
	function! QuickSave()
		:w!
	endfunction
	nnoremap <c-c> :call QuickSave()<CR>


" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-left> <C-w>h
nnoremap <A-down> <C-w>j
nnoremap <A-up> <C-w>k
nnoremap <A-right> <C-w>l
nnoremap <C-Left> :tabprevious<CR>                                                                            
nnoremap <C-Right> :tabnext<CR>


nnoremap <C-p> :FZF<CR>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit'
  \}


function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction


"Coc

inoremap <silent><expr> <c-space> coc#refresh()

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()



"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
